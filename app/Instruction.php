<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Instruction extends Model
{
    protected $fillable = ['title', 'description'];
    //
    public function exam() {
        return $this->hasOne('\App\Exam');
    }
}
