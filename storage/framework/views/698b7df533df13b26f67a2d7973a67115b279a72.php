<!-- JS Global Compulsory -->
<script type="text/javascript" src="<?php echo e(asset('public/assets/plugins/jquery/jquery.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('public/assets/plugins/jquery/jquery-migrate.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('public/assets/plugins/bootstrap/js/bootstrap.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('public/assets/js/plugins/jquery.countdown.js')); ?>"></script>
<script src="<?php echo e(asset('public/assets/js/loading.js')); ?>"></script>

<!-- JS Implementing Plugins -->
<script type="text/javascript" src="<?php echo e(asset('public/assets/plugins/sky-forms-pro/skyforms/js/jquery-ui.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('public/assets/plugins/back-to-top.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('public/assets/plugins/jquery-appear.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('public/assets/plugins/smoothScroll.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('public/assets/plugins/jquery.parallax.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('public/assets/plugins/counter/waypoints.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('public/assets/plugins/counter/jquery.counterup.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('public/assets/plugins/fancybox/source/jquery.fancybox.pack.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('public/assets/plugins/owl-carousel/owl-carousel/owl.carousel.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('public/assets/plugins/scrollbar/js/jquery.mCustomScrollbar.concat.min.js')); ?>"></script>
<?php echo $__env->yieldContent('pageplugins'); ?>
<!-- JS Page Level -->
<script type="text/javascript" src="<?php echo e(asset('public/assets/js/app.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('public/assets/js/plugins/fancy-box.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('public/assets/js/plugins/progress-bar.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('public/assets/js/plugins/owl-carousel.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('public/assets/js/plugins/style-switcher.js')); ?>"></script>
<script type="text/javascript">
    jQuery(document).ready(function() {
        App.init();
        App.initCounter();
        App.initParallaxBg();
        App.initScrollBar();
        FancyBox.initFancybox();
        App.initSidebarMenuDropdown();
        OwlCarousel.initOwlCarousel();
        StyleSwitcher.initStyleSwitcher();
        ProgressBar.initProgressBarHorizontal();
    });
</script>
<!--[if lt IE 9]>
<script src="<?php echo e(asset('public/assets/plugins/respond.js')); ?>"></script>
<script src="<?php echo e(asset('public/assets/plugins/html5shiv.js')); ?>"></script>
<script src="<?php echo e(asset('public/assets/plugins/placeholder-IE-fixes.js')); ?>"></script>
<![endif]-->
<!-- JS Google Analytics Tracking -->
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-84695501-1', 'auto');
    ga('send', 'pageview');

</script>